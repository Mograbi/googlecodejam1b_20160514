/*
 * digits.c
 *
 *  Created on: May 14, 2016
 *      Author: Moawiya
 */

#include "stdio.h"
#include "stdbool.h"

typedef enum {
	ZERO = 0, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE
} Number;
#define MAX_LEN 20
char s[MAX_LEN];
//	VEOTHNREEEFI
//  z    /   w    /    u    /    x    /     g    /
// zero one two three four five six seven eight nine
// 0111 111 101 11111 1101 1111 010 01111 11011 xxxx
// 1111 111 111 10111 0001 0001 000 01011 10001 1001	012
// 0101 101 011 	  1100 1101 011 01010 11110 0101
void reset_s() {
	int i = 0;
	for (; i < MAX_LEN; i++)
		s[i] = '$';
}
void remove_zero() {
	int i = 0;
	bool z_flag, e_flag, r_flag, o_flag;
	z_flag = e_flag = r_flag = o_flag = false;
	for (; i < MAX_LEN; i++) {
		switch (s[i]) {
		case 'Z':
			if (!z_flag) {
				s[i] = '$';
				z_flag = true;
			}
			break;
		case 'E':
			if (!e_flag) {
				s[i] = '$';
				e_flag = true;
			}
			break;
		case 'R':
			if (!r_flag) {
				s[i] = '$';
				r_flag = true;
			}
			break;
		case 'O':
			if (!o_flag) {
				s[i] = '$';
				o_flag = true;
			}
			break;
		}
	}
}
void remove_two() {
	int i = 0;
	bool t_flag, w_flag, o_flag;
	w_flag = t_flag = o_flag = false;
	for (; i < MAX_LEN; i++) {
		switch (s[i]) {
		case 'T':
			if (!t_flag) {
				s[i] = '$';
				t_flag = true;
			}
			break;
		case 'W':
			if (!w_flag) {
				s[i] = '$';
				w_flag = true;
			}
			break;
		case 'O':
			if (!o_flag) {
				s[i] = '$';
				o_flag = true;
			}
			break;
		}
	}
}
void remove_four() {
	int i = 0;
	bool f_flag, o_flag, u_flag, r_flag;
	f_flag = o_flag = u_flag = r_flag = false;
	for (; i < MAX_LEN; i++) {
		switch (s[i]) {
		case 'F':
			if (!f_flag) {
				s[i] = '$';
				f_flag = true;
			}
			break;
		case 'O':
			if (!o_flag) {
				s[i] = '$';
				o_flag = true;
			}
			break;
		case 'U':
			if (!u_flag) {
				s[i] = '$';
				u_flag = true;
			}
			break;
		case 'R':
			if (!r_flag) {
				s[i] = '$';
				r_flag = true;
			}
			break;
		}
	}
}
void remove_six() {
	int i = 0;
	bool s_flag, i_flag, x_flag;
	i_flag = s_flag = x_flag = false;
	for (; i < MAX_LEN; i++) {
		switch (s[i]) {
		case 'T':
			if (!s_flag) {
				s[i] = '$';
				s_flag = true;
			}
			break;
		case 'W':
			if (!i_flag) {
				s[i] = '$';
				i_flag = true;
			}
			break;
		case 'O':
			if (!x_flag) {
				s[i] = '$';
				x_flag = true;
			}
			break;
		}
	}
}
int main() {
	int i, T, size, t, index;
	int T;
	char c;
	scanf("%d", &T);
	for (t = 1; t <= T; t++) {
		index = 0;
		scanf("%c", &c);
		while (c != '\n') {
			s[index++] = c;
			scanf("%c", &c);
		}

	}
}
